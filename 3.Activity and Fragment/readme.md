**Необхідно знати**
- [Activity Lifecycle](https://developer.android.com/guide/components/activities/activity-lifecycle#java) і [тут](http://developer.alexanderklimov.ru/android/theory/lifecycle.php)
- [Context](https://medium.freecodecamp.org/mastering-android-context-7055c8478a22)
- [Application class](https://github.com/codepath/android_guides/wiki/Understanding-the-Android-Application-Class)	
- [Fragment Lifecycle](https://developer.android.com/guide/fragments/lifecycle) і [тут](http://developer.alexanderklimov.ru/android/theory/fragment-lifecycle.php)
- BackStack, FragmentManager, FragmentTransaction

**Завдання**
1. Створюємо одну Activity та виводимо логи у всіх методах Lifecycle. Запускаємо апку, згортаємо, розгортаємо, закриваємо, "вбиваємо". Дивимся в логи - розуміємо як працює.
2. Створюємо 3 Activity(одна вже із завдання 1): перша містить текстове поле із назвою "Activity 1" та кнопку із назвою "Next", друга - "Activity 2" та кнопки із назвами "Prev" та "Next", третя - поле із назвою "Activity 3" та кнопку із назвою "Prev". В кожній з них виводимо логи, аналогічно завданню 1. 
Робимо переходи вперед Activity1->Activity2->Activity3 та Activity3->Activity2->Activity1. Дивимся в логи.
3. Сворюємо 2 Fragment для Activity 2 (прибераємо в ній кнопки та текстове поле). Кожен із фрагментів має містити текстове поле із назвою фрагмента ("Fragment 1" та "Fragment 2") кнопки із назвами "Prev" та "Next". У фрагментах виводимо логи у всіх методах Lifecycle. 
Робимо переходи: Activity1->Activity2(Fragment1->Fragment2)->Activity3 та Activity3->Activity2(Fragment2->Fragment1)->Activity1. Дивимся в логи.
