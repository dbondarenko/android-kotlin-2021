**Знайомство з Kotlin**
Ваше завдання ознайомитися з основами мови (синтаксис та базові конструкції: об'єкти, класи, змінні, умовний оператор, цикли тощо)

Для ознайомлення:
1. docs [ENG](https://kotlinlang.org/docs/reference/basic-syntax.html)/[RUS](https://kotlinlang.ru/)
2. [31DaysOfKotlin](https://medium.com/androiddevelopers/31daysofkotlin-week-1-recap-fbd5a622ef86)
3. [курс](https://stepik.org/course/5448/syllabus)
4. [курс](https://itproger.com/course/kotlin)

